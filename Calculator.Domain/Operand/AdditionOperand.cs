﻿namespace Calculator.Domain.Operand;

public class AdditionOperand : IOperand
{
    private readonly string _supportedOperation = "+";

    public string GetOperand()
    {
        return _supportedOperation;
    }
    public bool CanExecute(string operand)
    {
        return operand == _supportedOperation;
    }

    public int Execute(int valA, int valB)
    {
        return valA + valB;
    }
}
