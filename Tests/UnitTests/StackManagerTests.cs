﻿using Calculator.Domain.StacksManager;
using Calculator.Infrastructure.StackManager;
using NUnit.Framework;

namespace UnitTests;

public class StackManagerTests
{
    private readonly IStackManager _stackManager;


    public StackManagerTests() => _stackManager = new StackManager();

    [Test]
    public void AddStackTest()
    {
        // Arrange
        var count = _stackManager.GetAllStacks().Count;

        //ACT 
        _stackManager.AddStack();
        var tasks = _stackManager.GetAllStacks();
        int expected = count+1;

        //Assert
        Assert.AreEqual(expected, tasks.Count);
    }

    [Test]
    public void GetStackTestShouldReturnStack()
    {
        // Arrange
        var stackid = _stackManager.AddStack();

        //ACT 
        var stack = _stackManager.Get(stackid);

        //Assert
        Assert.IsNotNull(stack);
    }

    [Test]
    public void GetStackTestShouldReturnNull()
    {
        //ACT
        var stack = _stackManager.Get(0);

        //Assert
        Assert.IsNull(stack);
    }

    [Test]
    public void ContainsKeyTestShouldReturnTrue()
    {
        // ACT
        var stackid = _stackManager.AddStack();

        //Assert
        Assert.IsTrue(_stackManager.ContainsStack(stackid));
    }

    [Test]
    public void ContainsKeyTestShouldReturnFalse()
    {
        //Assert
        Assert.IsFalse(_stackManager.ContainsStack(0));
    }

    [Test]
    public void RemoveStackTestShouldReturnTrue()
    {
        //Arrange
        var stackId = _stackManager.AddStack();

        //ACT
        var res = _stackManager.Remove(stackId);

        //Assert
        Assert.IsTrue(res);
    }
    [Test]
    public void RemoveStackTestShouldReturnFalse()
    {
        //Arrange
        var stackId = _stackManager.AddStack();

        //ACT
        var res = _stackManager.Remove(0);

        //Assert
        Assert.IsFalse(res);
    }


}
